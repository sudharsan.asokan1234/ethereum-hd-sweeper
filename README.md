# Ethereum HD Wallet Sweeper

This script allows you to iterate though an Ethereum HDWallet and sweep any/all coins found to a single address.

## Install

`git clone git@gitlab.com:dentino/ethereum-hd-sweeper.git`

`cd ethereum-hd-sweeper`

`npm install`

## Setup

There are two requirements needed to run the script:

1. An HD wallet xPrivate key or mnemonic 
2. An Ethereum provider

### HD wallet keys

`export XPRIV="xpriv8x21RrQH142....."`
 
 or:

 `export MNEMONIC="some nice words here..."`

### Ethereum Provider

The script uses [ethers.js](https://docs.ethers.io/ethers.js/html/index.html) which allows you to easily set different providers. The script can use Infura, Etherscan or a local node.

The provider you choose will depend on your use case and needs. It's a great idea to run your own node if possible. Check out [DAppnode](https://dappnode.io/) if you're looking for smooth way to do that.

**As far as I can tell, it's no longer required to set an API key for Infura or Etherscan as Ethers will handle that for you. However, it's recommend to set your own key(s) as this will allow you to track/log your calls through the respective interfaces.**

Example of setting API keys for Infura and/or Etherscan: 

`export INFURA_KEY="your_infura_api_key_here"`

`export ETHERSCAN_KEY="your_etherscan_api_key_here"`

Example for a local node using DAppnode:

`export LOCAL_URL='http://my.ethchain.dnp.dappnode.eth:8545'`

## Config

```javascript
// Adjust these as necessary
let gasPriceGwei = "2"; // in GWEI
let coldStorage = "0xF0Bf7545b8E8DDB6B9a0072a5CdBCC9F3db515c0"; // Address all coins will be swept to
let totalAccounts = 25000; // to sweep
let minimumBalance = 0.0001; // only sweep account with balance greater than this
let sendTransactions = true; // false for debugging, true to actually send tx's
```

Most of these should be self explanatory. **Make sure you change the `coldStorage` address** but you can also set `sendTransactions` to `false` for testing to make sure things are working as expected.

## Running the Script

`node HDsweeper.js`

or with logging for any accounts that have a balance:

`DEBUG=coins node HDsweeper.js`

or for verbose logging:

`DEBUG=verbose node HDsweeper.js`

## Notes

If you have any issues or questions please don't hesitate to open an issue. 

This script has been tested and used for upwards of 25K addresses. It does take a few minutes to run against that many addresses. If you're using Etherscan or Infura you might run into API limit issue depending on your plan. By design, it does check the balance for each of the `totalAccounts` addresses so you can easily know how many API calls to expect.

Happy sweeping :)
